﻿using UnityEngine;
using UnityEngine.Events;

namespace Game.Common
{
    public class TouchEvent : MonoBehaviour
    {
#pragma warning disable 0649
        [HideInInspector]
        public UnityEvent onOneTouchEnded, onTwoTouchEnded;

        public VectorEvent onOneTouch = new VectorEvent();
        public VectorEvent onOneTouchMove = new VectorEvent();

        public TwoVectorAndfloatEvent onTwoTouch = new TwoVectorAndfloatEvent();
        public TwoVectorAndfloatEvent onTwoTouchMove = new TwoVectorAndfloatEvent();
#pragma warning restore 0649
        private Camera cam;

        private void Start()
        {
            cam = Camera.main;
        }


        void Update()
        {
            Touch[] touches = Input.touches;

            if (touches.Length == 1)
            {
                DoOneTouch(touches[0]);
            }
            if (touches.Length == 2)
            {
                DoTwoTouch(touches);
            }
        }

        private void DoOneTouch(Touch touch)
        {
            // var touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                onTwoTouchEnded.Invoke();
                onOneTouch.Invoke(GetWordlPosition(touch.position));
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                onOneTouchMove.Invoke(GetWordlPosition(touch.position));
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                onOneTouchEnded.Invoke();
            }
        }

        private void DoTwoTouch(Touch[] touches)
        {
            var touch1 = touches[0];
            var touch2 = touches[1];

            if (touch1.phase == TouchPhase.Began || touch2.phase == TouchPhase.Began)
            {
                onOneTouchEnded.Invoke();
                UpdateTwoTouches(onTwoTouch, touch1, touch2);
            }
            else if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
            {
                UpdateTwoTouches(onTwoTouchMove, touch1, touch2);
            }
            else if (touch1.phase == TouchPhase.Ended 
                     || touch2.phase == TouchPhase.Ended 
                     || (touch1.phase == TouchPhase.Ended && touch2.phase == TouchPhase.Ended))
            {
                onTwoTouchEnded.Invoke();
            }
        }

        private void UpdateTwoTouches(TwoVectorAndfloatEvent action, Touch touch1, Touch touch2)
        {
            var pos1 = GetWordlPosition(touch1.position);
            var pos2 = GetWordlPosition(touch2.position);
            action.Invoke(pos1, pos2, (pos2 - pos1).magnitude);
        }


        private Vector3 GetWordlPosition(Vector3 scrennPoint)
        {
            return cam.ScreenToWorldPoint(scrennPoint);
        }
    }

    public class TwoVectorAndfloatEvent : UnityEvent<Vector3, Vector3, float> { }

    public class VectorEvent : UnityEvent<Vector3> { }
}
