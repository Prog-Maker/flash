﻿using UnityEngine;
using UnityEngine.Events;

namespace Game.Common
{
    public class InputManager : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent onButtonBack;

        protected virtual void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                onButtonBack.Invoke();
            }
        }
    }
}
