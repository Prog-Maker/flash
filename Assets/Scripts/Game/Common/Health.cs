﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Common
{
    public class Health : MonoBehaviour
    {
        [SerializeField]
        private float maxValue = 100;

        [SerializeField]
        private float currentValue = 0;

        [SerializeField]
        private float minHealthToDie = 0;

        public Action<float> actionOnDamage;

        public UnityEvent onDeath;

        void Start()
        {
            currentValue = maxValue;
        }

        public virtual void Damage(float damage)
        {
            currentValue -= damage;
            
            actionOnDamage?.Invoke(currentValue);

            if (currentValue <= minHealthToDie) Die();
        }

        protected virtual void Die()
        {
            onDeath.Invoke();
            Destroy(gameObject);
        }
    }
}
