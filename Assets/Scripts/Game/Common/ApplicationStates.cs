﻿using UnityEngine;
using UnityEngine.Events;

public class ApplicationStates : MonoBehaviour
{
    [SerializeField]
    private UnityEvent onAppHasFocus, onLostFocus;
    
    bool isPaused = false;
    public void QuitApp()
    {
        Application.Quit();
    }

    private void OnApplicationFocus(bool focus)
    {
        isPaused = !focus;

        onAppHasFocus.Invoke();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        isPaused = pauseStatus;

        onLostFocus.Invoke();
    }

    public void PauseGame()
    {
        onLostFocus.Invoke();
        Time.timeScale = 0;
    }

    public void UnPauseGame()
    {
        Time.timeScale = 1;
        onAppHasFocus.Invoke();
    }
}
