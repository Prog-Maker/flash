﻿using UnityEngine;

namespace Game.Common
{
    public class DirectionDetector : MonoBehaviour
    {
        [SerializeField]
        private DetectionType detectionType;

        [SerializeField]
        private LayerMask groundLayer;

        [SerializeField]
        private float circleRadius = 0.25f;

        [SerializeField]
        private HorizontalMoving horizontalMoving;

        private void FixedUpdate()
        {
            var colls = Physics2D.OverlapCircle(transform.position, circleRadius, groundLayer);
            switch (detectionType)
            {
                case DetectionType.Wall:
                    if(colls != null)
                    {
                        horizontalMoving?.ChangeDirection();
                    }
                    break;
                case DetectionType.Fall:
                    if (colls == null)
                    {
                        horizontalMoving?.ChangeDirection();
                    }
                    break;
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, circleRadius);
        }
    }

    public enum DetectionType
    {
        Wall, Fall
    }

}