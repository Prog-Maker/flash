﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Common
{
    public class GameTimer : MonoBehaviour
    {
        [SerializeField]
        private int maxTime = 30;

        [SerializeField]
        private string timerName = "Timer";

        [SerializeField]
        private UnityEvent onTimerCancel;

        private int currentTime = 0;

        private WaitForSeconds waiter = new WaitForSeconds(1);

        private void Start()
        {
            Restart();
        }

        public void Restart()
        {
            currentTime = maxTime;
            StartCoroutine(TimerStart());
        }

        private IEnumerator TimerStart()
        {
            while (currentTime != 0)
            {
                MMGameEvent.Trigger(GameEvents.Counter, timerName, currentTime);
                yield return waiter;
                currentTime--;
            }
           
            onTimerCancel.Invoke();
            MMGameEvent.Trigger(GameEvents.Counter, timerName, currentTime);
        }
    }
}
