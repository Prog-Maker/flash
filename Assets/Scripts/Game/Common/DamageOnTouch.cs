﻿using Game.Common;
using UnityEngine;

namespace Game.Flash
{
    public class DamageOnTouch : MonoBehaviour
    {
        [SerializeField]
        private float damage = 5;

        [SerializeField]
        private LayerMask layerMask;

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (1 << collision.gameObject.layer == layerMask.value)
            {
                var health = collision.GetComponent<Health>();
                
                if (health)
                {
                    health.Damage(damage);
                }
            }
        }

        public void SetDamage(float damage)
        {
            this.damage = damage;
        }
    }
}
