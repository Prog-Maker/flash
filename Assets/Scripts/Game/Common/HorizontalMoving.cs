﻿using UnityEngine;

namespace Game.Common
{
    public class HorizontalMoving : MonoBehaviour
    {
        [SerializeField]
        private float speed = 2f;

#pragma warning disable 0649
        [SerializeField]
        private Direction defaultDirection;
#pragma warning restore 0649

        private Rigidbody2D rb2D;

        private bool m_FacingRight = true;

        public float DirectionMove { get; private set; } = 1f;

        public Direction Direction => defaultDirection;

        private void Awake()
        {
            rb2D = GetComponent<Rigidbody2D>();
        }

        private void Start()
        {
            switch (defaultDirection)
            {
                case Direction.Left:
                    DirectionMove *= -1;
                    break;
            }
        }

        void Update()
        {
            rb2D.velocity = new Vector2(speed * DirectionMove, rb2D.velocity.y);
        }


        public void AddSpeed(float value)
        {
            speed += value;
        }


        public void ChangeDirection()
        {
            DirectionMove *= -1;
            Flip();
        }

        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }

    public enum Direction
    {
        Left, Rigth
    }
}
