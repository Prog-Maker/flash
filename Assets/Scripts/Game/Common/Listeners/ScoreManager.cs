﻿using System;
using UnityEngine;

namespace Game.Common
{
    public class ScoreManager : EventListernerMonoBehaviour
    {
        [SerializeField]
        private string scoreName = "Score";

        [SerializeField]
        private int mainScore = 0;

        public override void OnMMEvent(MMGameEvent eventType)
        {
            if(eventType.GameEventType == GameEvents.AddScore)
            {
                if(eventType.EventName == scoreName)
                {
                    mainScore += Convert.ToInt32(eventType.Arg);
                    
                    MMGameEvent.Trigger(GameEvents.UpdateScore, scoreName, mainScore);
                }
            }
        }
    }
}
