﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] prefabs;

    [SerializeField]
    private float delaySpawn = 2f;

    void Start()
    {
        InvokeRepeating(nameof(Spawn), 0, delaySpawn);
    }

    private void Spawn()
    {
        
        var index = Random.Range(0, prefabs.Length);

        var go = Instantiate(prefabs[index]);

        go.transform.position = transform.position;
    }
}
