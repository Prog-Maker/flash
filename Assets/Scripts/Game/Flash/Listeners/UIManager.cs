﻿using TMPro;
using UnityEngine;
using Game.Common;
using System;

namespace Game.Flash
{
    public class UIManager : EventListernerMonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField]
        private TextMeshProUGUI timerTextBox;

        [SerializeField]
        private TextMeshProUGUI scoreTextBox;
#pragma warning restore 0649
        [SerializeField]
        private string counterMane = "Timer";

        [SerializeField]
        private string scoreName = "Score";

        public override void OnMMEvent(MMGameEvent eventType)
        {
            if(eventType.GameEventType == GameEvents.Counter)
            {
                if(eventType.EventName == counterMane)
                {
                    timerTextBox?.SetText(Convert.ToString(eventType.Arg));
                }
            }

            if (eventType.GameEventType == GameEvents.UpdateScore)
            {
                if (eventType.EventName == scoreName)
                {
                    scoreTextBox?.SetText(Convert.ToString(eventType.Arg));
                }
            }
        }
    }
}
