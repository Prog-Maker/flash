﻿using Game.Common;
using UnityEngine;

namespace Game.Flash
{
    [RequireComponent(typeof(Health))]
    [RequireComponent(typeof(HorizontalMoving))]
    public class SpeedAndScaleController : MonoBehaviour
    {
        private Health health;

        private HorizontalMoving horizontalMoving;

        private float percent = 100;
       
        [SerializeField]
        private float minLocalScale = 1f;
       
        private float maxLocalScale = 1f;

        void Awake()
        {
            health = GetComponent<Health>();
            horizontalMoving = GetComponent<HorizontalMoving>();
        }

        private void Start()
        {
            health.actionOnDamage += ScaleAndSpeed;
            maxLocalScale = transform.localScale.y;
        }

        private void Update()
        {
            var scale = transform.localScale;
            
            scale.y = Mathf.Clamp(scale.y, minLocalScale, maxLocalScale);

            scale.x = horizontalMoving.DirectionMove * scale.y;

            if (horizontalMoving.Direction == Direction.Left) scale.x *= -1;

            scale.z = scale.y;

            transform.localScale = scale;
        }


        private void ScaleAndSpeed(float currnetHealth)
        {
            var scale = transform.localScale;

            scale.y = scale.y * currnetHealth / percent;

            var scaleValue = scale.y;

            scale.x = scaleValue;
            scale.z = scaleValue;

            transform.localScale = scale;

            horizontalMoving.AddSpeed(1);
        }

        private float SetLocalScale(float value, float currnetHealth)
        {
            return Mathf.Clamp(value, minLocalScale, value * currnetHealth / percent);
        }
    }
}
