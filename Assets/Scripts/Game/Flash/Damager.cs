﻿using DigitalRuby.LightningBolt;
using Game.Common;
using System.Linq;
using UnityEngine;

namespace Game.Flash
{
    [RequireComponent(typeof(TouchEvent))]
    public class Damager : MonoBehaviour
    {
#pragma warning disable 0649
        [SerializeField]
        private GameObject singleTouchDamager;

        [SerializeField]
        private GameObject twoTouchDamager;
#pragma warning restore 0649

        private TouchEvent touchEvent;

        private LightningBoltScript lightningBoltScript;

        private LineRenderer lineRenderer;

        private EdgeCollider2D edgeCollider;

        private DamageOnTouch damager;

        private Vector3[] points;

        private void Awake()
        {
            touchEvent = GetComponent<TouchEvent>();

            lightningBoltScript = twoTouchDamager.GetComponent<LightningBoltScript>();
            edgeCollider = twoTouchDamager.GetComponent<EdgeCollider2D>();
            lineRenderer = twoTouchDamager.GetComponent<LineRenderer>();
            damager = twoTouchDamager.GetComponent<DamageOnTouch>();

            touchEvent.onOneTouch.AddListener(ShowSingleDamager);
            touchEvent.onOneTouchMove.AddListener(MoveSingleDamager);
            touchEvent.onOneTouchEnded.AddListener(HideSingleDamager);

            touchEvent.onTwoTouch.AddListener(ShowTwiceDamager);
            touchEvent.onTwoTouchMove.AddListener(MoveTwiceDamager);
            touchEvent.onTwoTouchEnded.AddListener(HideTwiceDamager);
        }

        private void Start()
        {
            if (!singleTouchDamager) Debug.LogError("One Touch Damager GameObject is Null");
            if (!twoTouchDamager) Debug.LogError("Two Touch Damager GameObject is Null");
        }

        public void ShowSingleDamager(Vector3 pos)
        {
            pos.z = -1;
            singleTouchDamager.SetActive(true);
            singleTouchDamager.transform.position = pos;
        }

        public void ShowTwiceDamager(Vector3 pos1, Vector3 pos2, float distance)
        {
            twoTouchDamager.SetActive(true);

            UpdateTwiceDamager(pos1, pos2, distance);
        }


        public void MoveSingleDamager(Vector3 pos)
        {
            pos.z = -1;
            singleTouchDamager.transform.position = pos;
        }

        public void MoveTwiceDamager(Vector3 pos1, Vector3 pos2, float distance)
        {
            UpdateTwiceDamager(pos1, pos2, distance);

            Debug.Log(distance);
        }

        public void HideSingleDamager()
        {
            singleTouchDamager.SetActive(false);
        }

        public void HideTwiceDamager()
        {
            twoTouchDamager.SetActive(false);
        }

        private void UpdateTwiceDamager(Vector3 pos1, Vector3 pos2, float distance)
        {
            pos1.z = -1;
            pos2.z = -1;
            lightningBoltScript.StartPosition = pos1;
            lightningBoltScript.EndPosition = pos2;

            points = new Vector3[lineRenderer.positionCount];
            lineRenderer.GetPositions(points);

            lineRenderer.startWidth = 1 / distance * 20;
            lineRenderer.endWidth = 1 / distance * 20;

            damager.SetDamage(1 / distance * 50);

            edgeCollider.points = points.Select(p => (Vector2)p).ToArray();
        }
    }
}
