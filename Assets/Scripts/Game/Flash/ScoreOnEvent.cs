﻿using Game.Common;
using UnityEngine;

namespace Game.Flash
{
    public class ScoreOnEvent : MonoBehaviour
    {
        [SerializeField]
        private string scoreName = "Score";

        [SerializeField]
        private float count = 1;
        
        private Health health;

        private void Start()
        {
            health = GetComponent<Health>();

            health.onDeath.AddListener(() 
                                 => MMGameEvent.Trigger(GameEvents.AddScore, scoreName, count));
        }

    } 
}
